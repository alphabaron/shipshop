/** @modules */
import React, {PureComponent} from 'react'
import { StyleSheet, StatusBar, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { Button, Icon } from 'native-base'
import CameraRoll from "@react-native-community/cameraroll";

class Camera extends PureComponent {
    constructor(props){
        super();
        this.state = {
          flash: false
        }

    }
  render() {
    return (
      <><StatusBar barStyle="dark-content" backgroundColor="#000000"/>
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={this.state.flash ? RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          captureAudio={false}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes);
          }}
        />
       
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>

            <Button light bordered onPress={this.toggleFlash.bind(this)} transparent rounded style={styles.capture}>
             {this.state.flash ? <Icon style={{color: 'white'}} name="md-flash-off" type="Ionicons"/> : <Icon style={{color: 'white'}} name="md-flash" type="Ionicons"/>}
            </Button>
          
            <Button bordered light onPress={this.takePicture.bind(this)} transparent rounded style={styles.capture}>
              <Icon style={{color: 'white'}} name="scan1" type="AntDesign"/>
            </Button>

        </View>
      </View></>
    );
  }

  takePicture = async() => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      CameraRoll.saveToCameraRoll(data.uri, "photo")
      //console.log(data.uri);
      if(data.uri){
        this.props.navigation.navigate('Results', {
          image: data.uri,
          imagefull: data.base64
        })
      }
    }
  };

  toggleFlash = () => {
    this.setState({
      flash: !this.state.flash
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

export default Camera