import React, {useState, useEffect} from 'react'
import {Text,ScrollView, StatusBar, Image, View, NativeModules} from 'react-native'
import {Container, Content, Card, CardItem, CardSwiper, Spinner, Body, Right, Left, Button, Icon, Header} from 'native-base';
import CameraRoll from "@react-native-community/cameraroll";


const ImageResults = (props) => {

    const [isReady, setReady] = useState(false)
    const [imageUri, setImageUri] = useState("https://picsum.photos/200")
    const [predictions, setPredictions] = useState([])
    const [imagename, setImageName] = useState("")
    const [items, setItems] = useState([])
    const [selectedProduct, setSelectedProduct] = useState(null)

    function findMatches(img){
        // Default options are marked with *
        var image = img

        //console.log("this",image)

        var url = 'http://18.216.144.136/api/v1.0/extra/imageupload';

        var data = new FormData();

        data.append('itemImage', image);
        

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            method: "POST",
            body: data
        })
        .then(response => response.json())
        .then(response => {
            console.log("upload succes", response);

            if(response.code===200){
                console.log("url: ", response.responses)
                invokeImageRec(response.responses)
            }
            
        })
        .catch(error => {
            console.log("upload error", error);
        });
        
    }

    function invokeImageRec(imageurl){

        var url = 'https://vf21wx9gx1.execute-api.us-east-2.amazonaws.com/test/visualsearch/predict';

        //console.log("images: ", imageurl)
        var data = {
            "title": "aimg",
            "url": imageurl
        }

       //console.log("fuck",data)

        fetch(url, {
            method: "POST",
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(response => {
            
            console.log("predictions", response.matches);
            //setPredictions(response.matches)
            response.matches.map(pr => {
                predictions.push(pr)
            })
            //predictions.push(response.matches)
            console.log(predictions)
            
            
            //loop thru responses and do a search with  a contcat of tags
            if (predictions){
                let tags = []
                console.log("ll", predictions)
                predictions.map(item=> {
                    tags.push(item.tag)
                })

                strTags = tags.toString()
                console.log(strTags)
                data = {
                    keyword: strTags
                };

                fetch("http://18.216.144.136/api/v1.0/items/itemsearch", {
                    method: "POST",
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(response => response.json())
                .then(response => {
                    //console.log("Search",response)
                    
                    if(response.code === 200){
                        var pe = response.responses
                        console.log("rgerge",pe)
                        setItems(pe)

                        setReady(true)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
            }else{
                console.log("dfbfgnsxgn")
            }
            
        })
        .catch(error => {
            console.log("upload error", error);
        });
    }

    function addToCart(itemObj){
        
        console.log("passed",itemObj)
        
        setSelectedProduct(itemObj)


        if(selectedProduct){
            //console.log("selectedprodcut",selectedProduct)
            props.navigation.navigate('Checkout', {
                item: selectedProduct
            })
        }
    
			
		
    }

    useEffect(()=>{
        setTimeout(function(){

            var uu = props.navigation.getParam('image', imageUri)
            var imageFull = props.navigation.getParam('imagefull', imageUri)

            
            CameraRoll.getPhotos({first: 1, assetType: 'Photos',})
            .then(r => {
                
                setImageUri(uu);
                findMatches(imageFull)
                //setReady(true)
                
            })
            .catch((err) => {
                //Error Loading Image
             });
            
        }, 2000)
    }, [])


    if(isReady){
        return(
            <><StatusBar barStyle="light-content" backgroundColor="#ffffff"/>
            <Container>
                <ScrollView>
                    <Content>
                       
                        <Card transparent>
                            <CardItem cardBody style={{alignContent: 'center'}}>
                                <Body style={{alignItems: 'center', alignSelf: 'center', alignContent: 'center', justifyContent: 'center'}}>
                                <Image key={0} source={{uri: imageUri}} style={{width: '80%', height: 300}}/>
                                </Body>
                            </CardItem>
                            
                        </Card>
                        
                        <Text style={{fontSize: 20, marginTop: 40, marginLeft: 10}}>Possible Matches</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                            {items.length > 0 &&
                                items.map(item => {
                                    return(
                                        <View key={item._id} style={{marginRight: 5, marginLeft:5}}>
                                            <Card>
                                                <CardItem cardBody>
                                                    <Image source={{uri: item.itemImage[0]}} style={{width: 150, height: 150}}/>
                                                </CardItem>
                                                <CardItem footer>
                                                    <Body style={{alignContent: 'center'}}>
                                                        <Text>{item.itemName.slice(0,10)}</Text>
                                                        <Text style={{fontSize: 10}}>GHS {item.itemPrice}</Text>
                                                    </Body>
                                                </CardItem>
                                                <CardItem style={{marginLeft: 20, marginRight: 20}}>
                                                <Body>
                                                    <Button onPress={()=>{addToCart(item)}} small style={{backgroundColor: 'orange'}}>
                                                        <Text style={{margin: 10}}>Buy Now</Text>
                                                    </Button>
                                                </Body>
                                            </CardItem>
                                            </Card>
                                        </View>
                                    )
                                })
                            }

                                        
                        </ScrollView>

                        
                    </Content>
    
                  
                </ScrollView>
            </Container></>
        )
    }

    else{

        return(
            <><StatusBar barStyle="light-content" backgroundColor="#ffffff"/>
            <Container>
            <Content style={{alignContent: 'center'}}>
                <Spinner  style={{ alignSelf: 'center', marginTop:100 }}/>
                <Text style={{ alignSelf: 'center'}}>Searching for item...</Text>
            </Content>
            </Container></>
        )
    }

    
}


export default ImageResults