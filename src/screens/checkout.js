import React, {useState, useEffect} from 'react'
import {Text,ScrollView, StatusBar, Image, View, TextInput} from 'react-native'
import {Container, Content, Card, CardItem, CardSwiper, Spinner, Body, Right, Left, Button, Icon, Header, Form, Item, Input, Textarea, Title} from 'native-base';
import CameraRoll from "@react-native-community/cameraroll";


const Checkout = (props) => {

    const [isReady, setReady] = useState(true)
    const [fullname,setFullname] = useState("")
    const [email, setEmail] = useState("")
    const [phonenumber, setPhonenumber] = useState("")
    const [location, setLocation] = useState("")
    const [notes, setNotes] = useState("")
    const [validation, setValidation] = useState(false)
    const [items, setItems] = useState({})
    const [checkoutState, setCheckoutState] = useState(false)
    const [checkoutBtn, setCheckoutBtn] =useState("Checkout")

    function checkout() {
        console.log("clicked")
        //format phonenumber
        let formatedNumber;

        if(phonenumber.length === 10){
		    formatedNumber = '233'+phonenumber.substr(1)                  	
        }

        else if(phonenumber.length === 13){
            formatedNumber = phonenumber.substr(1)	
        }
        else {
            formatedNumber = phonenumber	
        }

        //let reg = /'^[0-9]$'/;
        setPhonenumber(formatedNumber)
            
        
        //send form
        console.log(items)
        items.itemQuantity = 1;
        var itemsArr=[]
        itemsArr.push(items)

        let orderData = {
            name: fullname,
            email: email,
            items: itemsArr,
            totalAmount: parseFloat(items.itemPrice),
            shopId: items.shopId,
            phonenumber: phonenumber,
            estimatedDeliveryPrice: 0,
            dropOffLocation: {
                latitude: 1,
                longitude: 1
            },
            payment_type: 'Mobile_Money',
            network: 'mtn-gh',
            itemsType: 0,
            notes: notes,
            country: 'GH',
            isDeliveryLocal: true,
            deliveryDetails: {
                estimateId: ""
            },
            response_page: "https://testshopaccount.curiashops.com/payments",
            dev: true
        };

        console.log(orderData)

        setCheckoutState(false)
        setCheckoutBtn("Loading...")

        const chkItemsExist = 'http://18.216.144.136/api/v1.0/orders/checkitems'
        const curiaAPI = `http://18.216.144.136/api/v1.0/orders`;
        console.log(chkItemsExist)
        fetch(chkItemsExist, {
            method: "POST",
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify(orderData)
        })
        .then(response => response.json())
        .then(response => {

            console.log(response)
            console.log(curiaAPI)

            if(response.code === 200){
                console.log('responding...')
                fetch(curiaAPI, {
                    method: "POST",
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify(orderData)
                })
                .then(response => response.json())
                .then(response => {
                    console.log('all items available')
                    console.log(response)
                   
                    setCheckoutBtn("Checkout")

                    if(response.code === 200){
                            console.log("complete")
                            setCheckoutState(true)
                            //complete
                            props.navigation.navigate('Complete', {
                                order: true
                            })
                    }

                    
                })
                .catch(err => {
                    console.log(err)
                    setCheckoutBtn("Checkout")
                })

            }

            else{
                console.log("fail")
                setCheckoutState(true)
                setCheckoutBtn("Try Again")
            }
        })

        

    

    }

    
    useEffect(()=>{

        var itemsn = props.navigation.getParam('item', {})
        console.log("thth", itemsn)

        setItems(itemsn)

    }, [])

   

    if(isReady){
        return(
            <><StatusBar barStyle="light-content" backgroundColor="#ffffff"/>
            <Container>
            <Header>
                <Left style={{flex: 1}}/>
                <Body style={{flex: 1, alignSelf: 'center', alignContent: 'center'}}>
                    <Title >Checkout</Title>
                </Body>
                <Right style={{flex: 1}} />
                </Header>

                <ScrollView>

                
                    
                <TextInput
                           placeholder="Full name"
                            textContentType="name"
                            style={{margin: 10, height: 40, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={(text)=>{setFullname(text)}}
                            />
                     <TextInput
                            textContentType="telephoneNumber"
                            placeholder="Phonenumber"
                            style={{margin: 10, height: 40, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={(text)=>{setPhonenumber(text)}}
                            />
                    <TextInput
                            textContentType="emailAddress"
                            placeholder="Email" 
                            onChangeText={(text)=>{setEmail(text)}}
                            style={{margin: 10, height: 40, borderColor: 'gray', borderWidth: 1 }}
                            />
                    <TextInput
                            textContentType="location"
                            placeholder="Location" 
                            onChangeText={(text)=>{setLocation(text)}}
                            style={{margin: 10, height: 40, borderColor: 'gray', borderWidth: 1 }}
                            />
                    <TextInput
                            multiline={true}
                            numberOfLines={3}
                            placeholder="Notes" 
                            onChangeText={(text)=>{setLocation(text)}}
                            style={{margin: 10, height: 40, borderColor: 'gray', borderWidth: 1, height: 80 }}
                            />

                    <Button success onPress={()=>{checkout()}} style={{alignSelf: 'center', width: '90%', margin: 20}}>
                            <Text style={{textAlign: 'center', flexDirection: 'row', flex:1}}>{checkoutBtn}</Text>
                    </Button>
                    
    
                  
                </ScrollView>
            </Container></>
        )
    }

    
}


export default Checkout