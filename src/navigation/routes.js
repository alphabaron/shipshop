import React from 'react';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer  } from 'react-navigation';

import Camera from '../screens/camera'
import ImageResults from '../screens/results'
import Complete from '../screens/complete'
import Checkout from '../screens/checkout'


const App = createStackNavigator(
  {
    Camera: Camera,
    Results: ImageResults,
    Complete: Complete,
    Checkout: Checkout
  }, {
      initialRouteName: 'Camera',
      headerMode: 'none'
  }
);

const MainNavigator = createAppContainer(App)

export default MainNavigator;
