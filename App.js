/** 
 * Shipshop
*/

/** @modules */
import React, {PureComponent} from 'react'
import {StatusBar} from 'react-native'

/** @modules */
import Camera from './src/screens/camera'
import ImageResults from './src/screens/results'
import Start from './src/screens/start'

import MainNavigator from './src/navigation/routes'

const App = () => {
  
    return (
      <MainNavigator/>
    );
  
}


export default App;
